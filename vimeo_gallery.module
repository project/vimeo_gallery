<?php
/********Constants***/

define('VIMEO_API_URL', 'http://www.vimeo.com/api/v2/');
define('VIMEO_SITE_URL', 'http://www.vimeo.com/');

/**
 * Implementation of hook_perm()
 */
function vimeo_gallery_permission() {
  return array(
    'administer vimeo_gallery' => array('title' => t('Administer Vimeo'),
      'description' => t('Configuration for Vimeo Gallery.'),
      'restrict access' => TRUE,
    ),
    'access vimeo_gallery' => array('title' => t('Access Vimeo'),
      'description' => t('Access Vimeo Gallery.'),
      'restrict access' => TRUE,
    ),
  );
}


/**
*Implementation of hook_menu()
*/
function vimeo_gallery_menu() {
  $video_path = variable_get('path_name', 'vimeo');
  $items['admin/config/media/vimeo_gallery'] = array(
    'title' => t('Vimeo Settings'),
    'description' => t('Configure required settings for Media: Vimeo.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vimeo_gallery_settings_form'),
    'access arguments' => array('administer vimeo_gallery'),
  );
  $items[$video_path] = array(
    'title' => t('Videos'),
    'page callback' => 'vimeo_gallery_page', 	
    'access arguments' => array('access vimeo_gallery'),
  );
  $items['vimeo/embed/%'] = array(
    'page callback' => 'vimeo_gallery_embed_page',
    'page arguments' => array(2),
    'access arguments' => array('access vimeo_gallery'),
  );
  return $items;
}

/**
 * Implementation of hook_theme()
 */
function vimeo_gallery_theme() {
  return array(
    'video_thumbnails' => array(
      'template' => 'video_thumbnails',
      'variables' => array('videos' => NULL),
    ),
  );
}

function vimeo_gallery_settings_form() {
  $performace = l(t('Clear cache'), 'admin/config/development/performance', array('html' => TRUE));
  $link = l(t('video'), variable_get('path_name', 'vimeo'), array('html' => TRUE));
  $form = array();
  $form['vimeo_help'] = array(
    '#markup' => t("After saving this form go to this link " . $link . " to check output."),
  );
  $form['vimeo_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Vimeo Username'),
    '#default_value' => variable_get('vimeo_username', ''),
    '#maxlength' => 64,
    '#description' => t('The name vimeo account'),
    '#required' => TRUE
  );
  $form['path_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter the videos path name'),
    '#default_value' => variable_get('path_name', 'vimeo'),
    '#maxlength' => 64,
    '#description' => t('Enter your own path name, and clear cache here ' . $performace . '.'),
  );
  $form['column_number'] = array(
    '#type' => 'textfield',
    '#title' => t('No of columns'),
    '#default_value' => variable_get('column_number', ''),
    '#maxlength' => 5,
    '#size' => 5,
    '#description' => t('Enter the number of videos to be displayed in a single row.'),
    '#required' => TRUE
  );
  $form['#validate'][] = 'vimeo_settings_form_validate';

  return system_settings_form($form);
}

function vimeo_settings_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['column_number'])) {
    form_set_error('column_number', t('Enter numeric value in No of columns.'));
    return false;
  }
}

// Curl helper function
function _curl_get($url) {
  $curl = curl_init($url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_TIMEOUT, 30);
  $return = curl_exec($curl);
  curl_close($curl);
  return $return;
}

/**
 * get list of video with given username
 */
function get_vimeo_gallerys() {
  $vimeo_username = variable_get('vimeo_username', '');
  $videos = '';
  if (!empty($vimeo_username)) {
    $api_endpoint =VIMEO_API_URL . $vimeo_username;
    // Load the video clips
    $videos = simplexml_load_string(_curl_get($api_endpoint . '/videos.xml'));
  }
  return $videos;
}

function vimeo_gallery_page() {
  // including vimeo_gallery related javascript files
  drupal_add_js(drupal_get_path('module', 'vimeo_gallery') . '/vimeo_gallery.js');
  drupal_add_js(array("vimeo_mod" => drupal_get_path('module', 'vimeo_gallery') . '/'), 'setting');

  $output = array();
  $title = '0';
  $perm = '';
  $col_num = variable_get('column_number', '1');
  if (user_access('administer vimeo_gallery')) {
    $perm=l(t('Configure'), 'admin/config/media/vimeo_gallery', array('html' => TRUE));
  }
  $message = t('Vimeo username is not yet configured.');
  $videos = get_vimeo_gallerys();
  if (!empty($videos)) {
    foreach ($videos->video as $video) {
      $photo_img = "<img src=" . $video->thumbnail_medium . " />";
      if (strlen($video->title)>'20') {
        $title=substr($video->title, 0, '20') . '.....';
      } 
      else {
        $title=$video->title;
      }
      $output[] = l($photo_img, 'vimeo/embed/' . $video->id, array('attributes' => array('class' => 'dialog'), 'html' => TRUE)) . '<br/><b>' . $title .'</b>';
    }
    $s = '206';
    $layout_width = $col_num*$s;
    $output1=array('vimeo' => $output, 'layout' => $layout_width);
    return theme('video_thumbnails', $output1);
  }
  else {
    return $message;
  }
}

/**
 * Process vimeo/embed/% url and displays the output 
 */
function vimeo_gallery_embed_page() {
  $output = '<object width="400" height="400">
             <param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" />
             <param name="movie" value="' . VIMEO_SITE_URL . 'moogaloop.swf?clip_id=' . arg(2) . '&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1&amp;autoplay=1" />
             <embed src="' . VIMEO_SITE_URL . 'moogaloop.swf?clip_id=' . arg(2) . '&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1&amp;autoplay=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="400" height="400"></embed>
             </object>';
  $output;
  exit;
}