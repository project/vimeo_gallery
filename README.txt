CONTENTS
In order for this module to work, you need to have php curl module enabled.
Remember to clear the cache after changing the videos path name

Installation Instructions
1. Download and unpack the module.
2. Place the module in your modules folder (this will usually be "sites/all/modules/").
3. Enable the module under admin/modules

Configuration:
-------------
Go to "Configuration" -> "Media" -> "Vimeo settings" to find all the configuration options.

Your videos output can be seen in structure ->menus->navigation->videos

Permissions:
-------------
Go to "People" -> "Permissions" ->"Vimeo Gallery" to find all the permissions.