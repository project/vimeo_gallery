(function($) {
    Drupal.behaviors.vimeo_gallery = {
	attach: function() {
	    $("#dialog-modal").dialog({
		modal: true,
		width: 'auto',
		position:'center',
		height: '480',
		autoOpen: false,
	    });
	    $('.dialog').click(function(event){
		var url = $(this).attr('href');
		$.get(url, function(data) {
		    $('#dialog-content').html(data);
		    
		})
		$("#dialog-modal").dialog("open");
		$("#dialog-content").html('<div style="width: 200px; height: 180px; border:0px solid red;"><img src="'+Drupal.settings.vimeo_mod+'loading.gif"/></div>');
		return false;
	    });
	}
    };
})(jQuery);


